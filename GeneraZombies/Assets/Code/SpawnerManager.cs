﻿using GeneraZombies.Misc;
using GeneraZombies.Units;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeneraZombies.Managers
{
    public class SpawnerManager : MonoBehaviour
    {
        public SpawnableUnit[] zombiesPrefabs;
        public SpawnPlace[] spawnPlaces;

        public static SpawnerManager Instance;

        public void Awake()
        {
            if (Instance == null)
            {
                Instance = gameObject.GetComponent<SpawnerManager>();
            }

            foreach (var prefab in zombiesPrefabs)
            {
                var unitType = prefab.unitType;

                //Inits a pool for each prefab
                if (!SpawnPoolManager.Instance.spawnUnitsPools.ContainsKey(unitType))
                {
                    SpawnPoolManager.Instance.spawnUnitsPools.Add(unitType, new SpawnPoolManager.SpawnableUnitsPool(prefab));
                }
            }
        }

        /// <summary>
        /// Gets a spawnUnit from the Pool
        /// </summary>
        public SpawnableUnit GetPooledSpawnUnit(SpawnUnitType spawnUnitType, Vector3 position)
        {
            return SpawnPoolManager.Instance.spawnUnitsPools[spawnUnitType].GetItem(position);
        }

    }

    #region POOL

    public class SpawnPoolManager
    {
        //Holds a pool for each spawn unit type
        public Dictionary<SpawnUnitType, SpawnableUnitsPool> spawnUnitsPools = new Dictionary<SpawnUnitType, SpawnableUnitsPool>();

        public SpawnPoolManager()
        {
            if (spawnUnitsPools == null)
                spawnUnitsPools = new Dictionary<SpawnUnitType, SpawnableUnitsPool>();
        }

        private static SpawnPoolManager instance;
        public static SpawnPoolManager Instance {
            get {
                if (instance == null)
                {
                    instance = new SpawnPoolManager();
                }
                else
                {
                    if (instance.spawnUnitsPools == null)
                        instance.spawnUnitsPools = new Dictionary<SpawnUnitType, SpawnableUnitsPool>();
                }
                return instance;
            }
        }

        public class SpawnableUnitsPool
        {
            public SpawnableUnit spawnableUnit;
            public int size;
            public bool canGrow;
            public List<SpawnableUnit> pooledItems;

            public GameObject hierarchyParent;

            public SpawnableUnitsPool(SpawnableUnit spawnUnit, int size = 1, bool canGrow = true)
            {
                this.spawnableUnit = spawnUnit;
                this.size = size;
                this.canGrow = canGrow;
                pooledItems = new List<SpawnableUnit>(size);

                hierarchyParent = new GameObject("Pool: " + spawnUnit.unitType.ToString());
            }

            public SpawnableUnit GetItem(Vector3 position)
            {
                SpawnableUnit spawnableUnit = null;
                for (int i = 0; i < pooledItems.Count; i++)
                {
                    if (pooledItems[i].IsInPool)
                    {
                        spawnableUnit = pooledItems[i];
                        pooledItems[i].IsInPool = false;
                        return spawnableUnit;
                    }
                }

                if (canGrow)
                {
                    pooledItems.Add(InstantiateItem(position));
                    spawnableUnit = pooledItems[pooledItems.Count - 1];
                    spawnableUnit.IsInPool = false;
                }
                else
                {
                    Debug.LogWarning("Pool limit reached!");
                }

                return spawnableUnit;
            }

            private SpawnableUnit InstantiateItem(Vector3 position)
            {
                var obj = Object.Instantiate(spawnableUnit.gameObject, position, Quaternion.identity);
                obj.transform.SetParent(Instance.spawnUnitsPools[spawnableUnit.unitType].hierarchyParent.transform);
                var spawnUnit = obj.GetComponent<SpawnableUnit>();
                spawnUnit.IsInPool = true;
                return spawnUnit;
            }
        }
    }

    #endregion
}

