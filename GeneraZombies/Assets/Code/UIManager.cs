﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace GeneraZombies.Managers
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance;

        public Camera uiCamera;

        public TextMeshProUGUI missionText;
        public TextMeshProUGUI zombiesTokillText;
        public TextMeshProUGUI restartText;
        public TextMeshProUGUI zombiesKilledText;
        public TextMeshProUGUI drawPathsInfo;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = gameObject.GetComponent<UIManager>();
            }

            drawPathsInfo.text = PathManager.Instance.drawPathLines ? "PRESS 'SPACE' TO HIDE PATHS" : "PRESS 'SPACE' TO SHOW PATHS";
        }

        private void Start()
        {
            GameManager.Instance.onGameStarted += ShowRestartText;
            GameManager.Instance.onGameFinished += ShowRestartText;
        }

        public void ShowRestartText()
        {
            restartText.text = "PRESS ANY KEY TO RESTART";
            restartText.gameObject.SetActive(GameManager.Instance.GameFinished);
        }
    }
}

