﻿using GeneraZombies.Managers;
using GeneraZombies.Units.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeneraZombies.Misc
{
    public class Shooter : MonoBehaviour
    {
        public float speedH = 2f;
        public float speedV = 2f;

        public float yaw = 0f;
        public float pitch = 0f;
        
        public float range = 100f;

        public ParticleSystem muzzleFlash;
        public ParticleSystem smoke;
        public ParticleSystem bullet;

        private Ray centerRay;
        private Camera uiCamera;
        private Vector2 centerScreen;

        private void Start()
        {
            uiCamera = UIManager.Instance.uiCamera;
            centerScreen = new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);
            centerRay = uiCamera.ScreenPointToRay(centerScreen);
        }

        void Update()
        {
            yaw += speedH * Input.GetAxis("Mouse X");
            pitch -= speedV * Input.GetAxis("Mouse Y");

            transform.eulerAngles = new Vector3(pitch, yaw, 0f);

            if (Input.GetButtonDown("Fire1"))
            {
                Shoot();
            }
        }

        private readonly RaycastHit[] hits = new RaycastHit[1];
        LayerMask unitsMask = (1 << 10); /*Units layer*/

        private void Shoot()
        {
            muzzleFlash.Play();
            smoke.Play();
            bullet.Play();

            centerRay = uiCamera.ScreenPointToRay(centerScreen);

            if (Physics.RaycastNonAlloc(centerRay, hits, range, unitsMask) > 0)
            {
                var zombie = hits[0].transform.GetComponent<Zombie>();

                if (zombie != null)
                {
                    zombie.Hit(hits[0].point);
                }
            }
        }
    }
}


