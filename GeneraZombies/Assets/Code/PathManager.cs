﻿using GeneraZombies.Misc;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeneraZombies.Managers
{
    public enum PathType
    {
        RANDOM = 0,
        PATH_0 = 1,
        PATH_1 = 2,
        PATH_2 = 3,
        PATH_3 = 4,
        PATH_4 = 5,
        PATH_5 = 6,
        PATH_6 = 7,
        PATH_7 = 8
    }

    [System.Serializable]
    public class PathData
    {
        public PathType pathID;
        public SpawnPlace spawner;
        public GoalPlace goal;
        [HideInInspector]
        public Vector3[] pathPositions;
    }

    public class PathManager : MonoBehaviour
    {
        public static PathManager Instance;
        
        private Vector3[] positions;

        public bool drawPathLines;
        public int pathPoints = 100;
        public GoalPlace[] goals;
        public List<PathData> pathDatas;

        public Dictionary<PathType, PathData> pathsInfo;

        public void Awake()
        {
            if (Instance == null)
            {
                Instance = gameObject.GetComponent<PathManager>();
            }

            positions = new Vector3[pathPoints];
            AddPathsData();
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                drawPathLines = !drawPathLines;
                UIManager.Instance.drawPathsInfo.text = drawPathLines ? "PRESS 'SPACE' TO HIDE PATHS" : "PRESS 'SPACE' TO SHOW PATHS";                
            }
        }

        private void AddPathsData()
        {
            pathsInfo = new Dictionary<PathType, PathData>();

            foreach (var path in pathDatas)
            {
                path.pathPositions = new Vector3[pathPoints];

                var pathToFollow = DrawLinearCurve(path.spawner.transform.position, path.goal.transform.position);

                for (int i = 0; i < pathToFollow.Length; i++)
                {
                    path.pathPositions[i] = pathToFollow[i];
                }

                if (!pathsInfo.ContainsKey(path.pathID))
                {
                    pathsInfo.Add(path.pathID, path);
                }
            }
        }

        /// <summary>
        /// Draws linear bezier curve
        /// </summary>
        /// <param name="point0">Spawner position.</param>
        /// <param name="point1">Goal position</param>
        /// <returns></returns>
        public Vector3[] DrawLinearCurve(Vector3 point0, Vector3 point1)
        {
            for (int i = 1; i < pathPoints + 1; i++)
            {
                float t = i / (float)pathPoints;
                positions[i - 1] = CalculateLinearBezierPoint(t, point0, point1);
            }

            return positions;
        }

        private Vector3 CalculateLinearBezierPoint(float t, Vector3 p0, Vector3 p1)
        {
            return p0 + t * (p1 - p0);
        }

        /// <summary>
        /// Draw quadratic bezier curve (one reference for curve)
        /// </summary>
        /// <param name="point0">Spawner position.</param>
        /// <param name="point1">Reference position for curve.</param>
        /// <param name="point2">Goal position.</param>
        /// <returns></returns>
        public Vector3[] DrawQuadraticCurve(Vector3 point0, Vector3 point1, Vector3 point2)
        {
            for (int i = 1; i < pathPoints + 1; i++)
            {
                float t = i / (float)pathPoints;
                positions[i - 1] = CalculateQuadraticBezierPoint(t, point0, point1, point2);
            }

            return positions;
        }

        private Vector3 CalculateQuadraticBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
        {
            float u = 1 - t;
            float tt = t * t;
            float uu = u * u;

            Vector3 point = uu * p0;
            point += 2 * u * t * p1;
            point += tt * p2;

            return point;
        }

        /// <summary>
        /// Draw cubic bezier curve (two references for curve)
        /// </summary>
        /// <param name="point0">Spawner position.</param>
        /// <param name="point1">Reference position for curve.</param>
        /// <param name="point2">Reference position for curve.</param>
        /// <param name="point3">Goal position.</param>
        /// <returns></returns>
        public Vector3[] DrawCubicCurve(Vector3 point0, Vector3 point1, Vector3 point2, Vector3 point3)
        {
            for (int i = 1; i < pathPoints + 1; i++)
            {
                float t = i / (float)pathPoints;
                positions[i - 1] = CalculateCubicBezierPoint(t, point0, point1, point2, point3);
            }

            return positions;
        }

        private Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            float u = 1 - t;
            float tt = t * t;
            float uu = u * u;
            float uuu = uu * u;
            float ttt = tt * t;

            Vector3 point = uuu * p0;
            point += 3 * uu * t * p1;
            point += 3 * u * tt * p2;
            point += ttt * p3;

            return point;
        }
    }
}



