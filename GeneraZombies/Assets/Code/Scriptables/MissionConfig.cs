﻿using GeneraZombies.Managers;
using GeneraZombies.Units;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeneraZombies.Configs
{
    [System.Serializable]
    public class MissionWaveData
    {
        public bool notActive;
        public float waveDelay;
        public bool randomLife;
        [Range(1, 4)]
        public int lifePoints;
        public bool randomAmount;
        [Range(1, 10)]
        public int amount;
        public float spawnRadious;
        public float spawnUnitDelay;
        public SpawnUnitType unitType;
        public float minSpeedFactor;
        public float distToMaxSpeedFactor;
        public UnitSpeedBehaviour speedBehavior;
        public PathType pathType;
        public UnitGoalAction finalAction;
    }

    [CreateAssetMenu(menuName = "Missions/MissionConfig")]
    public class MissionConfig : ScriptableObject
    {
        [Header("Mission Info")]
        public string missionName;
        public float missionDelay;
        [Header("Mission Waves Data")]
        public List<MissionWaveData> waves;
    }
}


