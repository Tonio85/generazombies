﻿using GeneraZombies.Configs;
using GeneraZombies.Misc;
using GeneraZombies.Units;
using GeneraZombies.Units.Characters;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GeneraZombies.Managers
{
    public class GameManager : MonoBehaviour
    {
        public MissionConfig[] missions;

        public static GameManager Instance;

        public int MissionIndex { get; set; }
        public int GlobalDeathCounter { get; set; }
        public bool GameFinished { get; set; }

        private MissionConfig currentMission;
        private int numberToKill;
        private int deathCounter;

        public System.Action onGameStarted = delegate { };
        public System.Action onGameFinished = delegate { };        

        public void Awake()
        {
            if (Instance == null)
            {
                Instance = gameObject.GetComponent<GameManager>();
            }
        }

        public void Start()
        {
            StartGame();
        }

        private void LateUpdate()
        {
            if (GameFinished && Input.anyKeyDown)
            {
                StartGame();
            }
        }

        public void StartGame()
        {
            GameFinished = false;
            onGameStarted();
            StartCoroutine(StartNextMission(missions[MissionIndex]));
        }
        
        public IEnumerator StartNextMission(MissionConfig mission)
        {
            currentMission = mission;
            numberToKill = 0;
           
            var thereIsActiveWave = mission.waves.First(m => !m.notActive) == null;

            /*Checks for waves*/
            if (mission.waves.Count == 0 || thereIsActiveWave)
            {
                MissionIndex++;
                if (MissionIndex < missions.Length)
                {
                    StartCoroutine(StartNextMission(missions[MissionIndex]));
                    yield break;
                }
                else
                {
                    UIManager.Instance.missionText.text = MissionIndex == 0 ? "NO WAVES DATA!" : "CONGRATULATIONS!";
                    UIManager.Instance.zombiesTokillText.text = string.Empty;
                    yield break;
                }
            }

            /*Gets total waves amount to kill*/
            foreach (var waveData in mission.waves)
            {
                if (waveData.notActive)
                    continue;

                if (waveData.randomAmount)
                {
                    waveData.amount = Random.Range(1, 6);
                }
                numberToKill += waveData.amount;
            }

            UIManager.Instance.missionText.text = "Mission: ";
            UIManager.Instance.zombiesTokillText.text = "Killed 0/0";

            /*Mission start delay*/
            yield return new WaitForSeconds(mission.missionDelay);

            var missionTitle = string.IsNullOrEmpty(mission.missionName) ? (MissionIndex + 1).ToString() : ": " + mission.missionName;
            UIManager.Instance.missionText.text = "Mission " + missionTitle;
            UIManager.Instance.zombiesTokillText.text = "Killed " + deathCounter + "/" + numberToKill;

            foreach (var waveData in mission.waves)
            {
                if (waveData.notActive)
                    continue;

                /*Wave start delay*/
                yield return new WaitForSeconds(waveData.waveDelay);

                for (int i = 0; i < waveData.amount; i++)
                {
                    /*Spawn unit delay*/
                    yield return new WaitForSeconds(waveData.spawnUnitDelay);

                    /*Spawn radious*/
                    float angle = i * Mathf.PI * 2 / waveData.amount;
                    Vector3 pos = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * waveData.spawnRadious;

                    Vector3 spawnPos = Vector3.zero;

                    if (PathManager.Instance.pathsInfo.ContainsKey(waveData.pathType))
                    {
                        spawnPos = PathManager.Instance.pathsInfo[waveData.pathType].spawner.transform.position;
                    }
                    else
                    {
                        waveData.pathType = PathType.RANDOM;
                    }

                    SpawnableUnit unitSpawned = SpawnerManager.Instance.GetPooledSpawnUnit(waveData.unitType, spawnPos + pos);

                    #region UNIT_CONFIG

                    if (unitSpawned is Zombie)
                    {
                        var zombie = (Zombie)unitSpawned;
                        zombie.GoalAction = waveData.finalAction;
                        if (PathManager.Instance.pathsInfo.ContainsKey(waveData.pathType))
                        {
                            zombie.PathData = PathManager.Instance.pathsInfo[waveData.pathType];
                            zombie.GoalPos = zombie.PathData.goal.transform.position + pos * 0.5f;
                        }
                       
                        if (waveData.pathType == PathType.RANDOM)
                        {                            
                            var spawnPlace = SpawnerManager.Instance.spawnPlaces[Random.Range(0, SpawnerManager.Instance.spawnPlaces.Length)];
                            spawnPos = spawnPlace.transform.position;

                            zombie.PathData = new PathData
                            {
                                pathID = PathType.RANDOM,
                                spawner = spawnPlace,
                                goal = PathManager.Instance.goals[Random.Range(0, PathManager.Instance.goals.Length)],
                                pathPositions = new Vector3[PathManager.Instance.pathPoints]
                            };

                            zombie.GoalPos = zombie.PathData.goal.transform.position + pos * 0.5f;
                        }

                        zombie.LifePoints = waveData.lifePoints;

                        if (waveData.randomLife)
                        {
                            zombie.LifePoints = Random.Range(1, 5);
                        }

                        zombie.IsFast = waveData.speedBehavior == UnitSpeedBehaviour.FAST;

                        if (waveData.speedBehavior == UnitSpeedBehaviour.RANDOM)
                        {
                            zombie.IsFast = Random.Range(0, 2) > 0;
                        }

                        zombie.transform.position = spawnPos + pos;

                        zombie.MinSpeedFactor = (waveData.minSpeedFactor == 0 ? 0.3f : waveData.minSpeedFactor);
                        zombie.DistToMaxSpeedFactor = (waveData.distToMaxSpeedFactor == 0 ? 0.35f : waveData.distToMaxSpeedFactor);

                        StartCoroutine(zombie.StartMoving());
                    }
                  
                    #endregion                    
                }
            }
        }

        public void CheckForNextMission()
        {
            deathCounter++;

            GlobalDeathCounter++;
            UIManager.Instance.zombiesKilledText.text = "Total zombies killed: " + GlobalDeathCounter;

            UIManager.Instance.zombiesTokillText.text = "Killed " + deathCounter + "/" + numberToKill;

            if (deathCounter == numberToKill)
            {
                MissionIndex++;
                deathCounter = 0;
                if (MissionIndex < missions.Length)
                {
                    StartCoroutine(StartNextMission(missions[MissionIndex]));
                }
                else
                {
                    StartCoroutine(EndGame());
                }
            }
        }

        private IEnumerator EndGame()
        {
            UIManager.Instance.missionText.text = "CONGRATULATIONS!";
            UIManager.Instance.zombiesTokillText.text = string.Empty;

            yield return new WaitForSeconds(1f);

            MissionIndex = 0;

            GameFinished = true;
            onGameFinished();
        }
    }
}

