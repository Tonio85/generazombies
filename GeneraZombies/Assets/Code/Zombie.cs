﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using GeneraZombies.Managers;
using System.Linq;

namespace GeneraZombies.Units.Characters
{
    public class Zombie : SpawnableUnit
    {
        public Animator zombieAnimator;
        public LineRenderer lineRenderer;
        public ParticleSystem blood;
        public SpriteRenderer lifeSprite;
        public RectTransform lifeMaskRect;

        public float SpeedFactor { get; set; }
        public float MinSpeedFactor { get; set; }
        public float DistToMaxSpeedFactor { get; set; }
        public Vector3 GoalPos { get; set; }
        public PathData PathData { get; set; }
        public Vector3 CurrentMovePos { get; set; }
        public bool IsFast { get; set; }
        public float LifePoints { get; set; }
        public UnitGoalAction GoalAction { get; set; }


        private bool goalReached;
        private bool isMoving;
        private bool isDead;

        private readonly Collider[] colliders = new Collider[2];
        private Vector3[] obstaclesPos;

        private RectTransform lifeSpriteRect;
        private RectTransform lifeParent;

        private float lifeStep;
        private float lifeMaskStep;

        private Vector3 lifeStepMove;
        private Vector3 lifeInitialSize;
        private Vector3 lifeRectInitialPos;
        private Vector3 lifeMaskInitialScale;

        private CapsuleCollider colliderCap;

        public System.Action onDied = delegate { };

        protected override void Awake()
        {
            base.Awake();

            lifeSpriteRect = lifeSprite.GetComponent<RectTransform>();
            lifeParent = lifeSpriteRect.parent.GetComponent<RectTransform>();

            lifeRectInitialPos = lifeSpriteRect.localPosition;
            lifeMaskInitialScale = lifeMaskRect.localScale;
            lifeInitialSize = lifeSprite.size;

            colliderCap = GetComponent<CapsuleCollider>();

            if (blood == null)
            {
                blood = GetComponentInChildren<ParticleSystem>();
            }
        }

        int pathIndex = 0;

        public void Update()
        {
            /*Face life to camera*/
            lifeParent.transform.rotation = Quaternion.LookRotation(lifeSprite.transform.position - UIManager.Instance.uiCamera.transform.position);

            /*Draws path line*/
            if (PathManager.Instance.drawPathLines && !isDead)
            {
                lineRenderer.positionCount = PathData.pathPositions.Length;
                lineRenderer.SetPositions(PathData.pathPositions);
            }
            else
            {
                lineRenderer.positionCount = 0;
            }

            if (goalReached || !isMoving)
                return;
           
            var totalDistance = PathData.spawner.transform.position - PathData.pathPositions[PathData.pathPositions.Length - 1];
            var distanceToGoal = GoalPos - transform.position;

            /*The closer to the goal the faster value (blendtree move)*/
            SpeedFactor = distanceToGoal.magnitude
                .Remap(totalDistance.magnitude * DistToMaxSpeedFactor, totalDistance.magnitude, 1f, MinSpeedFactor);

            SpeedFactor = Mathf.Clamp(SpeedFactor, MinSpeedFactor, 1f);

            if (pathIndex == PathData.pathPositions.Length)
            {
                /*Reached last path index but still hasn't reached goal*/
                if (distanceToGoal.magnitude > 1f)
                {
                    return;
                }

                transform.position = GoalPos;

                goalReached = true;
                isMoving = false;

                zombieAnimator.SetBool("IsMoving", false);

                FinalAction();
                return;
            }

            CurrentMovePos = PathData.pathPositions[pathIndex];

            var distanceToGo = CurrentMovePos - transform.position;

            /*Initial radious pos might get further away*/
            if (distanceToGo.magnitude <= (pathIndex == 0 ? 5f : 0.5f))
            {
                pathIndex++;
            }

            if (IsFast)
            {
                SpeedFactor = 1f;
            }

            transform.position = Vector3.Lerp(transform.position, CurrentMovePos, Time.deltaTime * 2.5f * SpeedFactor);

            var cameraPos = UIManager.Instance.uiCamera.transform.position;
            var posToLook = new Vector3(cameraPos.x, 0f, cameraPos.z);

            Quaternion lookRot =
                   Quaternion.LookRotation(CurrentMovePos - transform.position);

            transform.rotation =
            Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime /*0.5f*/);

            zombieAnimator.SetFloat("Movement", Mathf.Clamp(SpeedFactor, 0, 1));
        }

        LayerMask obstacleMask = (1 << 9); /*Obstacles*/
        private int CheckClosestObstacles()
        {
            var obstaclesNum = Physics.OverlapSphereNonAlloc(transform.position, 100f, colliders, obstacleMask);

            obstaclesPos = new Vector3[2];

            List<Collider> notNullColliders = new List<Collider>();

            if (obstaclesNum > 0)
            {
                #region GETS_TWO_CLOSEST_OBSTACLES

                /*var notNullColliders = */
                notNullColliders = colliders.Where(c => c != null).ToList(); ;
                var orderedColliders = notNullColliders.OrderBy(c => Vector3.Distance(transform.position, c.transform.position));

                obstaclesPos[0] = orderedColliders.ElementAt(0).transform.position;
                obstaclesPos[1] = orderedColliders.ElementAt(1).transform.position;             

                #endregion
            }

            return notNullColliders.Count;
        }

        /// <summary>
        /// Sets the path with bezier curves depending on obstacles reference positions
        /// </summary>
        private void CheckForPath()
        {
            var obstaclesAmount = CheckClosestObstacles();

            /*Linear curve*/
            if (obstaclesAmount == 0)
            {
                var newPath = PathManager.Instance.DrawLinearCurve(transform.position, GoalPos);

                for (int i = 0; i < newPath.Length; i++)
                {
                    PathData.pathPositions[i] = newPath[i];
                }
                return;
            }

            /*Quadratic*/
            if (obstaclesAmount == 1)
            {
                var newPath = PathManager.Instance.DrawQuadraticCurve(transform.position,
                    obstaclesPos[0], GoalPos);

                for (int i = 0; i < newPath.Length; i++)
                {
                    PathData.pathPositions[i] = newPath[i];
                }
            }
            /*Cubic*/
            else if (obstaclesAmount == 2)
            {
                var newPath = PathManager.Instance.DrawCubicCurve(transform.position,
                   obstaclesPos[0], obstaclesPos[1], GoalPos);

                for (int i = 0; i < newPath.Length; i++)
                {
                    PathData.pathPositions[i] = newPath[i];
                }
            }
        }

        public IEnumerator StartMoving()
        {
            onDied -= GameManager.Instance.CheckForNextMission;
            onDied += GameManager.Instance.CheckForNextMission;

            isDead = false;
            goalReached = false;
            pathIndex = 0;

            /*Face life to camera*/
            lifeParent.transform.rotation = Quaternion.LookRotation(lifeSprite.transform.position - UIManager.Instance.uiCamera.transform.position);

            /*Face unit to camera*/
            var cameraPos = UIManager.Instance.uiCamera.transform.position;
            var posToLook = new Vector3(cameraPos.x, 0f, cameraPos.z);

            Quaternion lookRot = Quaternion.LookRotation(posToLook - transform.position);
            transform.rotation = lookRot;

            CheckForPath();

            CurrentMovePos = PathData.pathPositions[pathIndex];

            zombieAnimator.SetBool("IsMoving", true);

            /*Draws path line*/
            if (PathManager.Instance.drawPathLines)
            {
                lineRenderer.positionCount = PathData.pathPositions.Length;
                lineRenderer.SetPositions(PathData.pathPositions);
            }

            zombieAnimator.speed = 1f;

            #region LIFE_UI_INITIAL_SET

            lifeSpriteRect.localPosition = lifeRectInitialPos;
            lifeSprite.size = lifeInitialSize;
            lifeMaskRect.localScale = lifeMaskInitialScale;

            lifeStep = lifeSprite.size.x / 4f;

            lifeSprite.size = new Vector2(lifeStep * LifePoints, lifeSprite.size.y);

            lifeStepMove = new Vector3(-lifeStep, 0f, 0f);

            lifeMaskStep = lifeMaskRect.localScale.x / 4f; /*Max life points*/
            var lifeTotalSteps = lifeMaskStep * LifePoints;
            var xPos = lifeTotalSteps.RoundToDecimalDigits(2);
            lifeMaskRect.localScale = new Vector3(xPos, lifeMaskRect.localScale.y, lifeMaskRect.localScale.z);

            lifeParent.gameObject.SetActive(true);

            #endregion

            colliderCap.enabled = true;

            yield return new WaitForSeconds(0.5f);

            /*Might be killed before start moving*/
            if (!isDead)
            {
                isMoving = true;
            }
        }

        private void FinalAction()
        {
            switch (GoalAction)
            {
                case UnitGoalAction.ATTACK:
                    zombieAnimator.SetTrigger("Attack");
                    break;
                case UnitGoalAction.BITING:
                    zombieAnimator.SetTrigger("Bite");
                    break;
            }
        }

        public void Hit(Vector3 worldPos)
        {
            /*Positions blood effect at hit pos*/
            blood.transform.localPosition = transform.InverseTransformPoint(worldPos);
            blood.Play();

            if (isDead)
            {
                return;
            }

            LifePoints--;

            lifeSpriteRect.localPosition += lifeStepMove;

            if (LifePoints <= 0)
            {
                onDied();

                isDead = true;
                isMoving = false;

                lifeSprite.transform.parent.gameObject.SetActive(false);

                zombieAnimator.SetTrigger("Die");

                colliderCap.enabled = false;

                lineRenderer.positionCount = 0;

                return;
            }

            isMoving = false;
            zombieAnimator.SetTrigger("Hit");
        }

        #region ANIMATION_EVENTS

        public void HitAnimFinish()
        {
            if (isDead)
            {
                isMoving = false;

                if (!IsInPool)
                {
                    StartCoroutine(BackToPool(0.1f));
                }

                return;
            }

            StartCoroutine(BackToMoving());
        }

        public void DieAnimFinish()
        {
            HideMeshes();
            zombieAnimator.speed = 0f;
            zombieAnimator.Play("Idle", 0, 0f);

            pathIndex = 0;
            goalReached = false;

            StartCoroutine(BackToPool(0.1f));
        }

        #endregion 

        private IEnumerator BackToMoving()
        {
            yield return new WaitForSeconds(0.1f);

            if (!goalReached && !isDead)
                isMoving = true;
        }
    }
}

public static class ExtensionMethods
{
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    public static float RoundToDecimalDigits(this float value, int digits)
    {
        float mult = Mathf.Pow(10.0f, (float)digits);
        return Mathf.Round(value * mult) / mult;
    }

}
