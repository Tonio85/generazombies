﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeneraZombies.Units
{
    public enum SpawnUnitType
    {
        ZOMBIE_GIRL = 0,
        ZOMBIE_COP = 1
    }

    public enum UnitGoalAction
    {
        ATTACK = 0,
        BITING = 1
    }

    public enum UnitSpeedBehaviour
    {
        RANDOM = 0,
        INCREASING = 1,
        FAST = 2
    }

    public class SpawnableUnit : MonoBehaviour
    {
        private bool isInPool;
        public bool IsInPool {
            get { return isInPool; }
            set {
                isInPool = value;
                gameObject.SetActive(!isInPool);
                foreach (var mesh in meshes)
                {
                    mesh.enabled = !isInPool;
                }
            }
        }

        public SkinnedMeshRenderer[] meshes;

        public SpawnUnitType unitType;

        protected virtual void Awake()
        {
            meshes = GetComponentsInChildren<SkinnedMeshRenderer>();
        }

        public void HideMeshes()
        {
            foreach (var mesh in meshes)
            {
                mesh.enabled = false;
            }
        }

        public IEnumerator BackToPool(float delay)
        {
            yield return new WaitForSeconds(delay);

            IsInPool = true;
        }
    }
}

